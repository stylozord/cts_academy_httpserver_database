package com.opencloud.slee.example.http.ping.db;

import com.opencloud.slee.example.http.ping.db.exception.SendException;
import com.opencloud.slee.resources.dbquery.*;

import javax.slee.facilities.Tracer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.TimeoutException;

public class SendSyncRequest extends SendRequest {

    private final DatabaseQueryProvider dbProvider;
    private final Tracer tracer;

    public SendSyncRequest(DatabaseQueryProvider dbProvider, Tracer tracer) {
        this.dbProvider = dbProvider;
        this.tracer = tracer;
    }

    public void doSelectCommand(String httpKey, StringBuilder responseText) throws SendException {

        final QueryInfo queryInfo = new SelectHttpKey(httpKey);
        DatabaseFutureResult result = null;

        try {
            result = dbProvider.sendQuery(queryInfo);
            ResultSet selectResult = result.getResultSet(5000);  // **BLOCKING**
            if (selectResult.next()) {
                String httpValue = selectResult.getString(1);
                responseText.append(httpValue);
            } else {
                responseText.append(String.format("No http_value found for '%s'\n", httpKey));
            }
            selectResult.close();
        } catch (NoDataSourcesAvailableException e) {
            throw new SendException("No data sources available", e);
        } catch (SQLException e) {
            throw new SendException("Sql exception", e);
        } catch (InsufficientResourcesException e) {
            throw new SendException("Insufficient resources", e);
        } catch (TimeoutException e) {
            throw new SendException("Timeout while waiting for query result", e);
        } catch (DatabaseQueryException e) {
            throw new SendException("Database query exception", e);
        } finally {
            if (result != null) result.close();
        }
    }
}
