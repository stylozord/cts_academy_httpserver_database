package com.opencloud.slee.example.http.ping.db;


import com.opencloud.slee.example.http.ping.db.exception.SendException;

public interface SendRequestInterface {
    void doSelectCommand(String httpKey, StringBuilder responseText) throws SendException;

}
