package com.opencloud.slee.example.http.ping.db;

import com.opencloud.slee.example.http.ping.db.exception.SendException;
import com.opencloud.slee.resources.dbquery.*;

import javax.slee.ActivityContextInterface;
import javax.slee.SbbLocalObject;
import javax.slee.facilities.Tracer;
import javax.slee.resource.StartActivityException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SendAsyncRequest extends SendRequest {

    private final DatabaseQueryProvider dbProvider;
    private final DatabaseQueryActivityContextInterfaceFactory dbACIFactory;
    private final SbbInterface sbb;
    private final SbbLocalObject sbbLocalObject;
    private final Tracer tracer;

    public SendAsyncRequest(DatabaseQueryProvider dbProvider, DatabaseQueryActivityContextInterfaceFactory dbACIFactory, SbbInterface sbb, SbbLocalObject sbbLocalObject, Tracer tracer) {
        this.dbProvider = dbProvider;
        this.sbb = sbb;
        this.dbACIFactory = dbACIFactory;
        this.sbbLocalObject = sbbLocalObject;
        this.tracer = tracer;
    }

    public void doSelectCommand(String httpKey, StringBuilder responseText) throws SendException {
        sendQuery(new SelectHttpKey(httpKey), responseText);
    }

    public void onDatabaseResult(DatabaseResultEvent result, ActivityContextInterface aci, StringBuilder responseText, boolean rollback) throws SendException {
        final QueryInfo queryInfo = result.getQueryInfo();
        try {
            appendResponse(result, queryInfo, responseText);
        } catch (SQLException e) {
            throw new SendException("SQLException", e);
        }
    }

    private void sendQuery(QueryInfo queryInfo, StringBuilder responseText) throws SendException {
        try {
            DatabaseQueryActivity nonTxnActivity = dbProvider.createActivity();
            ActivityContextInterface aci = dbACIFactory.getActivityContextInterface(nonTxnActivity);
            nonTxnActivity.sendQuery(queryInfo);
            aci.attach(sbbLocalObject);
            responseText.append("Submitted ").append(queryInfo).append(" on non-transactional activity\n");
        } catch (StartActivityException e) {
            throw new SendException("Could not start SLEE activity", e);
        } catch (NoDataSourcesAvailableException e) {
            throw new SendException("No DataSources available", e);
        }
    }

    private void appendResponse(DatabaseResultEvent result, QueryInfo queryInfo, StringBuilder responseText) throws SQLException {
        if (queryInfo instanceof SelectHttpKey) {
            String httpKey = ((SelectHttpKey) queryInfo).getHttpkey();
            ResultSet selectResult = result.getResultSet();
            if (selectResult.next()) {
                String httpValue = selectResult.getString(1);
                responseText.append(String.format("Http Value %s for key %s \n", httpValue, httpKey));
            } else {
                responseText.append(String.format("No Http Value found for key '%s'\n", httpKey));
            }
        }
    }


}
