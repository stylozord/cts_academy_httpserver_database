package com.opencloud.slee.example.http.ping.db;

import com.opencloud.slee.resources.dbquery.Query;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author OpenCloud
 */
abstract class SendRequest implements SendRequestInterface {

    protected class SelectHttpKey extends Query {
        SelectHttpKey(String http_key) {
            super("SELECT http_value FROM test.HTTP WHERE http_key = ?");
            this.http_key = http_key;
        }

        public void setParameters(PreparedStatement stmt) throws SQLException {
            stmt.setString(1, http_key);
        }

        @Override
        public String toString() {
            return String.format("SELECT http_value FROM test.HTTP WHERE http_key = '%s'", http_key);
        }

        String getHttpkey() {
            return http_key;
        }

        private String http_key;

    }

}
