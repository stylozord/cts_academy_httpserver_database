package com.opencloud.slee.example.http.ping;

import com.opencloud.slee.example.http.ping.db.SbbInterface;
import com.opencloud.slee.example.http.ping.db.SendAsyncRequest;
import com.opencloud.slee.example.http.ping.db.SendRequestInterface;
import com.opencloud.slee.example.http.ping.db.SendSyncRequest;
import com.opencloud.slee.example.http.ping.db.exception.SendException;
import com.opencloud.slee.example.http.ping.services.common.BaseSbb;
import com.opencloud.slee.resources.dbquery.DatabaseQueryActivityContextInterfaceFactory;
import com.opencloud.slee.resources.dbquery.DatabaseQueryProvider;
import com.opencloud.slee.resources.dbquery.DatabaseResultEvent;
import com.opencloud.slee.resources.http.HttpRequest;
import com.opencloud.slee.resources.http.HttpResponse;
import com.opencloud.slee.resources.http.IncomingHttpRequestActivity;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.slee.ActivityContextInterface;
import javax.slee.RolledBackContext;
import javax.slee.SbbContext;
import javax.slee.facilities.TraceLevel;
import javax.slee.facilities.Tracer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * @slee.sbb
 *   id="exampledbquerytest"
 *   name="Database Query Test SBB"
 *   vendor="OpenCloud"
 *   version="1.0"
 *
 * @slee.library-ref
 *   library-name="OpenCloud Service Common classes"
 *   library-vendor="OpenCloud"
 *   library-version="1.0"
 *
 * @slee.ra-type-binding
 *   resource-adaptor-type-name="Example"
 *   resource-adaptor-type-vendor="OpenCloud"
 *   resource-adaptor-type-version="@EXAMPLE_RATYPE_VERSION@"
 *
 * @slee.ra-type-binding
 *   resource-adaptor-type-name="Database Query"
 *   resource-adaptor-type-vendor="OpenCloud"
 *   resource-adaptor-type-version="@DBQUERY_RATYPE_VERSION@"
 *   activity-context-interface-factory-name="slee/resources/dbquery/activitycontextinterfacefactory"
 *   resource-adaptor-object-name="slee/resources/dbquery/provider"
 *   resource-adaptor-entity-link="slee/resources/dbquery"
 *
 * @slee.service
 *   description="Database Query Test Service"
 *   name="Database Query Test Service"
 *   vendor="OpenCloud"
 *   version="1.0"
 *   default-priority="0"
 *
 * @author OpenCloud
 */
public abstract class PingSbb extends BaseSbb implements SbbInterface {
    private DatabaseQueryProvider dbProvider;
    private DatabaseQueryActivityContextInterfaceFactory dbACIFactory;
    private Tracer tracer;
    private StringBuilder responseText = new StringBuilder();

    private static final String header =
            "<html><head><title>HTTP Ping SBB</title></head><body>\n";

    private static final String footer =
            "</body></html>";

    public void setSbbContext(SbbContext sbbContext) {

        super.setSbbContext(sbbContext);
        try {
            final Context env = (Context) new InitialContext().lookup("java:comp/env");
            dbProvider = (DatabaseQueryProvider) env.lookup("slee/resources/dbquery/provider");
            dbACIFactory = (DatabaseQueryActivityContextInterfaceFactory) env.lookup("slee/resources/dbquery/activitycontextinterfacefactory");
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }


    protected String getTraceType() {
        return "Ping";
    }

    /**
     * Handles HTTP GET request
     */
    public void onGetRequest(HttpRequest request, ActivityContextInterface aci) {
        if (isFinestTraceable()) finest("onGetRequest: received request: " + request);
        try {
            IncomingHttpRequestActivity activity =
                    (IncomingHttpRequestActivity) aci.getActivity();

            saveActivity(aci);
            trace(TraceLevel.WARNING, "Saved ACI: " + aci);

            try {
                String query = request.getRequestURL().getQuery();
                trace(TraceLevel.WARNING, "query request " + query);
                trace(TraceLevel.WARNING, " QUERY VALUE: " + getKeyFromQuery(query));
                createRequestSender(true).doSelectCommand(getKeyFromQuery(query), responseText);
            } catch (SendException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            warning("unable to generate response", e);
        }
    }

    private SendRequestInterface createRequestSender(boolean async) {
        return async ?
                new SendAsyncRequest(dbProvider, dbACIFactory, this, getSbbLocalObject(), tracer) :
                new SendSyncRequest(dbProvider, getTracer());
    }

    private static List<String> sort(Iterator<String> iter) {
        List<String> list = new ArrayList<String>();
        while (iter.hasNext()) {
            list.add(iter.next());
        }
        Collections.sort(list);
        return list;
    }

    public void sbbExceptionThrown(Exception exception, Object event, ActivityContextInterface aci) {
    }

    private String getKeyFromQuery(String query) {
        return query.substring(query.indexOf("=") + 1);
    }

    public void onDatabaseResult(DatabaseResultEvent result, ActivityContextInterface aci) {
        try {
            StringBuilder responseText = new StringBuilder();
            ((SendAsyncRequest) createRequestSender(true)).onDatabaseResult(result, aci, responseText, getRollback());
            trace(TraceLevel.WARNING, "RESPONSE:" + responseText.toString());

            IncomingHttpRequestActivity activity = getHttpAci();
            HttpResponse response = activity.createResponse(200, "OK");

            sendResponse(response, responseText.toString());

        } catch (SendException e) {
            severe(e.getMessage(), e);
        }
    }

    private void sendResponse(HttpResponse httpResponse, String message) {
        try {

            StringBuilder content = new StringBuilder(header);
            content.append("<h1>Ping SBB</h1>\n");
            content.append("<p>SbbID: ").append(getSbbContext().getSbb());
            content.append("<p>HTTP version: ").append(getHttpAci().getRequest().getVersion().toString());
            content.append("<p>URL: ").append(getHttpAci().getRequest().getRequestURL());
            content.append("<p> HTTP QUERY VALUE: ").append(message);

            content.append("</pre>\n");
            content.append("<p>Request content length: " + getHttpAci().getRequest().getContentLength() + " bytes\n<pre>");
            if (getHttpAci().getRequest().getContentLength() > 0)
                content.append("<p>Raw content:\n<pre>").append(getHttpAci().getRequest().getContentAsString()).append("</pre>\n");
            content.append(footer);
            httpResponse.setContentAsString("text/html; charset=\"utf-8\"", content.toString());

            if (isFinestTraceable()) finest("onGetRequest: sending response: " + httpResponse);
            getHttpAci().sendResponse(httpResponse);

            //TODO onActivityEnd() -- add new method
            //TODO onServiceStarted()
            //TODO clearActivity();
        } catch (IOException e) {
            warning(e.getMessage());
        }
    }

    /**
     * @slee.cmp-method
     */
    public abstract boolean getSyncMode();

    /**
     * @slee.cmp-method
     */
    public abstract void setSyncMode(boolean b);

    /**
     * @slee.cmp-method
     */
    public abstract byte[] getTransaction();

    /**
     * @slee.cmp-method
     */
    public abstract void setTransaction(byte[] txn);

    /**
     * @slee.cmp-method
     */
    public abstract boolean getRollback();

    /**
     * @slee.cmp-method
     */
    public abstract void setRollback(boolean b);

    public void sbbRolledBack(RolledBackContext context) {
    }

    /**
     * @slee.cmp-method
     */
    public abstract ActivityContextInterface getTransactionalActivity();

    /**
     * @slee.cmp-method
     */
    public abstract void setTransactionalActivity(ActivityContextInterface a);

    public void saveActivity(ActivityContextInterface aci) {
        setTransactionalActivity(aci);
    }

    public IncomingHttpRequestActivity getHttpAci() {
        return (IncomingHttpRequestActivity) (getTransactionalActivity().getActivity());
    }

    public void clearActivity() {
        setTransactionalActivity(null);
    }
}

