package com.opencloud.slee.example.http.ping.db;

import javax.slee.ActivityContextInterface;

public interface SbbInterface {
    void saveActivity(ActivityContextInterface aci);

    void clearActivity();
}
