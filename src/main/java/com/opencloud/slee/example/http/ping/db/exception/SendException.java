package com.opencloud.slee.example.http.ping.db.exception;

/**
 * @author OpenCloud
 */
public class SendException extends Exception {

    public SendException(String message) {
        super(message);
    }

    public SendException(String message, Throwable cause) {
        super(message, cause);
    }
}
